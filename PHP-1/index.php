<!DOCTYPE html>
<html class="" lang="en">
<head prefix="og: http://ogp.me/ns#">
  <title>HTML-CSS-PHP</title>
  <link rel="stylesheet" href="../highlight/styles/a11y-light.min.css">
  <script src="../highlight/highlight.min.js"></script>
  <script>document.addEventListener('DOMContentLoaded', (event) => {
      document.querySelectorAll('pre code').forEach((el) => {
        hljs.highlightElement(el);
      });
    });</script>

</head>
  <body>
    <ul>
      <li><a href="php-0-php.php">PHP 0 php</a></li>
      <li><a href="php-1-echo.php">PHP 1 echo</a></li>
      <li><a href="php-2-configuration.php">PHP 2 Configuration</a></li>
      <li><a href="php-3-variables.php">PHP 3 variables</a></li>
      <li><a href="php-4-conditions.php">PHP 4 conditions</a></li>
      <li><a href="php-5-boucles.php">PHP 5 boucles</a></li>
      <li><a href="php-6-tableau.php">PHP 6 tableau</a></li>
      <li><a href="php-7-fonctions.php">PHP 7 fonctions</a></li>
      <li><a href="php-8-erreurs.php">PHP 8 erreurs</a></li>
      <li><a href="php-9-include.php">PHP 9 include</a></li>
    </ul>
  </body>

</html>
