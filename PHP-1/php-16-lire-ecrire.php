<?php include('../header.php'); ?>
<title>Lire et écrire dans un fichier PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Lire et écrire dans un fichier PHP</h2>
<pre>
    <code>
    <?php
    // 1 : on ouvre le fichier
    $monfichier = fopen('compteur.txt', 'r+');

    // 2 : on fera ici nos opérations sur le fichier...

    // 3 : quand on a fini de l'utiliser, on ferme le fichier
    fclose($monfichier);
    ?>
    </code>
  </pre>
<p>r : Ouvre le fichier en lecture seule</p>
<p>r+ :  ouvre le fichier en lecture et écriture</p>
<p>a :  ouvre le fichier en lecture et écriture ou le crée</p>
<p>a+ :  ouvre le fichier en lecture et écriture ou le créé</p>
<p>fgets : lit le fichier ligne par ligne</p>
<p>fgetc : lit le fichier caractère par caractère</p>


<?php include('../footer.php'); ?>
