<?php include('../header.php'); ?>
  <title>Variables</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Type de variables</h2>
<p>Chaines de charactères (string)</p>
<pre>
  <code>

    <?php
    $variable = "Mon \"nom\" est Mateo21";
    $variable = 'Je m\'appelle Mateo21';
    ?>
  </code>
</pre>
<p>Nombres entiers (int)</p>
<pre>
  <code>
    <?php
    $age_du_visiteur = 17;
    ?>

  </code>
</pre>
<p>Nombres décimaux (float)</p>

<p>Booléen (Bool)</p>
<pre>
  <code>
    <?php
    $je_suis_un_zero = true;
    $je_suis_bon_en_php = false;
    ?>

  </code>
</pre>

<p>Pas de valeur (Null)</p>

<pre>
  <code>
    <?php
    $pas_de_valeur = NULL;
    ?>
  </code>
</pre>

<h2>Concatenation</h2>
<pre>
  <code>
    <?php
    $age_du_visiteur = 17;
    echo "Le visiteur a ";
    echo $age_du_visiteur;
    echo " ans";
    ?>

    <?php
    $age_du_visiteur = 17;
    echo 'Le visiteur a ' . $age_du_visiteur . ' ans';
    ?>
  </code>
</pre>
<h2>Calculs</h2>
<pre>
  <code>
    <?php
    $nombre = 2 + 4; // $nombre prend la valeur 6
    $nombre = 5 - 1; // $nombre prend la valeur 4
    $nombre = 3 * 5; // $nombre prend la valeur 15
    $nombre = 10 / 2; // $nombre prend la valeur 5

    // Allez on rajoute un peu de difficulté
    $nombre = 3 * 5 + 1; // $nombre prend la valeur 16
    $nombre = (1 + 2) * 2; // $nombre prend la valeur 6
    ?>
  </code>
  <p>Modulo - affiche le reste d'une division</p>
  <pre>
    <code>
      <?php
      $nombre = 10 % 5; // $nombre prend la valeur 0 car la division tombe juste
      $nombre = 10 % 3; // $nombre prend la valeur 1 car il reste 1
      ?>
    </code>
  </pre>
</pre>

<?php include('../footer.php'); ?>

