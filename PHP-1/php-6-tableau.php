<?php include('../header.php'); ?>
  <title>Tableaux</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Tableaux</h2>
<p>Tableau associatif</p>
<p>Tableau numéroté</p>

<h2>Tableau numéroté</h2>
<pre>
    <code>
      <?php
      // La fonction array permet de créer un array
      $prenoms = array ('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');
      ?>

      <?php
      $prenoms[] = 'François'; // Créera $prenoms[0]
      $prenoms[] = 'Michel'; // Créera $prenoms[1]
      $prenoms[] = 'Nicole'; // Créera $prenoms[2]
      ?>

      <?php
      echo $prenoms[1];
      ?>
    </code>
  </pre>

<h2>Tableau associatif</h2>
<pre>
  <code>
    <?php
    // On crée notre array $coordonnees
    $coordonnees = array (
      'prenom' => 'François',
      'nom' => 'Dupont',
      'adresse' => '3 Rue du Paradis',
      'ville' => 'Marseille');
    ?>

    <?php
    echo $coordonnees['ville'];
    ?>
  </code>
</pre>
<h2>Parcourir un tableau</h2>
<p>Boucle FOR</p>
<pre>
  <code>
    <?php
    // On crée notre array $prenoms
    $prenoms = array ('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');

    // Puis on fait une boucle pour tout afficher :
    for ($numero = 0; $numero < 5; $numero++)
    {
      echo $prenoms[$numero] . '<br />'; // affichera $prenoms[0], $prenoms[1] etc.
    }
    ?>
  </code>
</pre>
<p>Boucle foreach</p>
<pre>
  <code>
    <?php
    $prenoms = array ('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');

    foreach($prenoms as $element)
    {
      echo $element . '<br />'; // affichera $prenoms[0], $prenoms[1] etc.
    }
    ?>

    <?php
    $coordonnees = array (
      'prenom' => 'François',
      'nom' => 'Dupont',
      'adresse' => '3 Rue du Paradis',
      'ville' => 'Marseille');

    foreach($coordonnees as $element)
    {
      echo $element . '<br />';
    }
    ?>

    <?php
    $coordonnees = array (
      'prenom' => 'François',
      'nom' => 'Dupont',
      'adresse' => '3 Rue du Paradis',
      'ville' => 'Marseille');

    foreach($coordonnees as $cle => $element)
    {
      echo '[' . $cle . '] vaut ' . $element . '<br />';
    }
    ?>

    <?php
    $coordonnees = array (
      'prenom' => 'François',
      'nom' => 'Dupont',
      'adresse' => '3 Rue du Paradis',
      'ville' => 'Marseille');

    echo '<pre>';
    print_r($coordonnees);
    echo '</pre>';
    ?>
  </code>
</pre>
<h2>Rechercher dans un tableau</h2>
<pre>
  <code>
    <?php
    $coordonnees = array (
      'prenom' => 'François',
      'nom' => 'Dupont',
      'adresse' => '3 Rue du Paradis',
      'ville' => 'Marseille');

    if (array_key_exists('nom', $coordonnees))
    {
      echo 'La clé "nom" se trouve dans les coordonnées !';
    }

    if (array_key_exists('pays', $coordonnees))
    {
      echo 'La clé "pays" se trouve dans les coordonnées !';
    }

    ?>

    <?php
    $fruits = array ('Banane', 'Pomme', 'Poire', 'Cerise', 'Fraise', 'Framboise');

    if (in_array('Myrtille', $fruits))
    {
      echo 'La valeur "Myrtille" se trouve dans les fruits !';
    }

    if (in_array('Cerise', $fruits))
    {
      echo 'La valeur "Cerise" se trouve dans les fruits !';
    }
    ?>
  </code>
</pre>
<p>Récupérer la clé</p>
<pre>
  <code>
    <?php
    $fruits = array ('Banane', 'Pomme', 'Poire', 'Cerise', 'Fraise', 'Framboise');

    $position = array_search('Fraise', $fruits);
    echo '"Fraise" se trouve en position ' . $position . '<br />';

    $position = array_search('Banane', $fruits);
    echo '"Banane" se trouve en position ' . $position;
    ?>
  </code>
</pre>

<?php include('../footer.php'); ?>


