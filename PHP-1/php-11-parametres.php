<?php include('../header.php'); ?>
<title>Paramètres PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>paramètres PHP</h2>

<pre>
  <code>


    <?php
    if (isset($_GET['prenom']) AND isset($_GET['nom'])) // On a le nom et le prénom
    {
      echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !';
    }
    else // Il manque des paramètres, on avertit le visiteur
    {
      echo 'Il faut renseigner un nom et un prénom !';
    }
    ?>

    <p>3 paramètres</p>

    <?php
    if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter']))
    {
      for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
      {
        echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
      }
    }
    else
    {
      echo 'Il faut renseigner un nom, un prénom et un nombre de répétitions !';
    }
    ?>

    <p>Sécuriser</p>
    <?php
    if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter']))
    {
      // 1 : On force la conversion en nombre entier
      $_GET['repeter'] = (int) $_GET['repeter'];

      // 2 : Le nombre doit être compris entre 1 et 100
      if ($_GET['repeter'] >= 1 AND $_GET['repeter'] <= 100)
      {
        for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
        {
          echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
        }
      }
    }
    else
    {
      echo 'Il faut renseigner un nom, un prénom et un nombre de répétitions !';
    }
    ?>
  </code>
</pre>



<?php include('../footer.php'); ?>


