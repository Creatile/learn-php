<?php include('../header.php'); ?>
  <title>Fonctions</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Fonctions</h2>
<a href="https://www.php.net/manual/fr/funcref.php">Liste des fonctions</a>
<pre>
  <code>
    <?php
    calculCube(4);
    ?>

    <?php
    fonctionImaginaire(17, 'Vert', true, 41.7);
    ?>
    <p>Retourne une valeur, on la met dans une variable</p>
    <?php
    $volume = calculCube(4);
    ?>
    <p>Calcule le nombre de caractères</p>
    <?php
    $phrase = 'Bonjour tout le monde ! Je suis une phrase !';
    $longueur = strlen($phrase);


    echo 'La phrase ci-dessous comporte ' . $longueur . ' caractères :<br />' . $phrase;
    ?>
    <p>Recherche et remplace</p>
    <?php
    $ma_variable = str_replace('b', 'p', 'bim bam boum');

    echo $ma_variable;
    ?>
    <p>Récupérer une date</p>
    <?php
    // Enregistrons les informations de date dans des variables

    $jour = date('d');
    $mois = date('m');
    $annee = date('Y');

    $heure = date('H');
    $minute = date('i');

    // Maintenant on peut afficher ce qu'on a recueilli
    echo 'Bonjour ! Nous sommes le ' . $jour . '/' . $mois . '/' . $annee . 'et il est ' . $heure. ' h ' . $minute;
    ?>

    <?php
    function DireBonjour($nom)
    {
      echo 'Bonjour ' . $nom . ' !<br />';
    }

    DireBonjour('Marie');
    DireBonjour('Patrice');
    DireBonjour('Edouard');
    DireBonjour('Pascale');
    DireBonjour('François');
    DireBonjour('Benoît');
    DireBonjour('Père Noël');
    ?>

    <?php
    // Ci-dessous, la fonction qui calcule le volume du cône
    function VolumeCone($rayon, $hauteur)
    {
      $volume = $rayon * $rayon * 3.14 * $hauteur * (1/3); // calcul du volume
      return $volume; // indique la valeur à renvoyer, ici le volume
    }

    $volume = VolumeCone(3, 1);
    echo 'Le volume d\'un cône de rayon 3 et de hauteur 1 est de ' . $volume;
    ?>
  </code>
</pre>


<?php include('../footer.php'); ?>


