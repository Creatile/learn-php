<?php include('../header.php'); ?>
  <title>Include PHP</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Includes PHP</h2>
<pre id="embedded_ace_code">
  <code class="language-php hljs">
    <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon super site</title>
    </head>

    <body>

    <?php include("entete.php"); ?>

    <?php include("menus.php"); ?>

    <!-- Le corps -->

    <div id="corps">
        <h1>Mon super site</h1>

        <p>
            Bienvenue sur mon super site !<br />
            Vous allez adorer ici, c'est un site génial qui va parler de... euh... Je cherche encore un peu le thème de mon site. :-D
        </p>
    </div>

    <!-- Le pied de page -->

    <?php include("pied_de_page.php"); ?>

    </body>
</html>
  </code>
</pre>


<?php include('../footer.php'); ?>


