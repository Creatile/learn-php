<?php include('../header.php'); ?>
<title>Include PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Includes PHP</h2>
<pre id="embedded_ace_code">
  <code class="language-php hljs">
    <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon super site</title>
    </head>

    <body>

    <pre>
      <code>
        <?php
        $nombreDeMessagesParPage = 20;

        $retour = $bdd->query('SELECT COUNT(*) AS nb_messages FROM livre');
        $donnees = $retour->fetch();
        $totalDesMessages = $donnees['nb_messages'];

        $nombreDePages  = ceil($totalDesMessages / $nombreDeMessagesParPage);

        echo 'Page : ';
        for ($page_actuelle = 1 ; $page_actuelle <= $nombreDePages ; $page_actuelle++)
        {
          echo '<a href="livre.php?page=' . $page_actuelle . '">' . $page_actuelle . '</a> ';
        }
        ?>

        <?php
        for ($ligne = 1 ; $ligne <= 100 ; $ligne++)
        {
          if ($ligne % 2 == 0)
          {
            echo $ligne . ' : <strong>ligne paire</strong>';
          }
          else
          {
            echo $ligne . ' : <em>ligne impaire</em>';
          }

          echo '<br />';
        }
        ?>

        <?php
        /*
        Script "Questionnaire de satisfaction"
            Par M@teo21

        Dernière modification : 20 août XXXX
        */

        // On vérifie d'abord s'il n'y a pas de champ vide
        if ($_POST['description'] == NULL OR $_POST['mail'] == NULL)
        {
          echo 'Tous les champs ne sont pas remplis !';
        }
        else // Si c'est bon, on enregistre les informations dans la base
        {
          $bdd->prepare('INSERT INTO enquete VALUES (\'\', ?, ?)');
          $bdd->execute(array($_POST['description'], $_POST['mail']));

          // Puis on envoie les photos

          for ($numero = 1 ; $numero <= 3 ; $numero++)
          {
            if ($_FILES['photo' . $numero]['error'] == 0)
            {
              if ($_FILES['photo' . $numero]['size'] < 500000)
              {
                move_uploaded_file($_FILES['photo' . $numero]['tmp_name'], $numero . '.jpg');
              }
              else
              {
                echo 'La photo ' . $numero . 'n\'est pas valide.<br />';
                $probleme = true;
              }
            }
          }

          // Enfin, affichage d'un message de confirmation si tout s'est bien passé

          if (!(isset($probleme)))
          {
            echo 'Merci ! Les informations ont été correctement enregistrées !';
          }
        }
        ?>
      </code>
    </pre>


<?php include('../footer.php'); ?>


