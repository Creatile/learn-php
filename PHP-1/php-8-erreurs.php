<?php include('../header.php'); ?>
  <title>Erreurs PHP</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Erreurs PHP</h2>

<p>Parse Erreur</p>
<p>Erreur de point virgule ou point</p>
<pre>
  <code>
    $id_news = 5
  </code>
</pre>
<p>Undefined function ;</p>
<p>Fonction n'existe pas ou pas chargée</p>
<pre>
  <code>
    Fatal Error: Call to undefined function: fonction_inconnue() in fichier.php on line 27
  </code>
</pre>
<p>Wrong parameter count.</p>
<pre>
  <code>
    $fichier = fopen("fichier.txt");
  </code>
</pre>
<p>Paramètre non défini</p>
<p>Headers already sent</p>
<pre>
  <code>
    Cannot modify header information - headers already sent by ...

        <html>
    <?php session_start(); ?>
  </code>
</pre>
<p>Maximum time executed</p>
<pre>
  <code>
    <?php
    $nombre = 5;
    while ($nombre == 5)
    {
      echo 'Zéro ';
    }
    ?>
  </code>
</pre>

<?php include('../footer.php'); ?>


