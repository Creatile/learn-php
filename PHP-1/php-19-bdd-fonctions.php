<?php include('../header.php'); ?>
  <title>Base de donnée fonctions PHP</title>
  <meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
  </head>
  <body>
  <h2>Base de donnée fonctions PHP</h2>

  <pre>
    <code>
      <?php
      $reponse = $bdd->query('SELECT AVG(prix) AS prix_moyen FROM jeux_video');

      while ($donnees = $reponse->fetch())
      {
        echo $donnees['prix_moyen'];
      }

      $reponse->closeCursor();

      ?>

      SELECT UPPER(nom) AS nom_maj FROM jeux_video

      SELECT LOWER(nom) AS nom_min FROM jeux_video

      SELECT LENGTH(nom) AS longueur_nom FROM jeux_video

      SELECT ROUND(prix, 2) AS prix_arrondi FROM jeux_video

      SELECT AVG(prix) AS prix_moyen FROM jeux_video WHERE possesseur='Patrick'

      SELECT AVG(prix) AS prix_moyen, console FROM jeux_video GROUP BY console
    </code>
  </pre>



<?php include('../footer.php'); ?>



