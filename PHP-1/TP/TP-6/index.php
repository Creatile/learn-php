<?php
session_start();
$pseudo = isset($_SESSION['pseudo'])? $_SESSION['pseudo'] : "";
?>
<!DOCTYPE html>
<html class="" lang="en">
<head prefix="og: http://ogp.me/ns#">
</head>
<body>
<form id="chat-form" action="minichat_post.php" method="POST">
  <div class="form-item">
    <label for="pseudo">Pseudo</label>
    <input type="text" name="pseudo" value="<?php echo $pseudo; ?>">
  </div>
  <div class="form-item">
    <label for="message">Message</label>
    <input type="text" name="message">
  </div>
  <div class="form-action">
    <input type="submit" name="send" value="Send">
  </div>
  <?php echo $_SESSION['erreur']; ?>

</form>
<div>
  <?php
  try
  {
    $bdd = new PDO('mysql:host=localhost;dbname=jvideo;charset=utf8', 'root', 'root');
  }
  catch(Exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }

  $req = $bdd->prepare('SELECT pseudo, message FROM chat ORDER BY ID DESC LIMIT 0, 10');
  $req->execute();

  echo '<div>';
  while ($donnees = $req->fetch())
  {
    echo '<div>' . $donnees['pseudo'] . ' (' . $donnees['message'] . ')</div>';
  }
  echo '</div>';

  $req->closeCursor();

  ?>
</div>
</body>
</html>