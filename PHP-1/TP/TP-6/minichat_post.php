<?php
session_start();
try {
  $bdd = new PDO('mysql:host=localhost;dbname=jvideo;charset=utf8', 'root', 'root');
} catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}

$pseudo = isset($_POST['pseudo']) ? htmlspecialchars($_POST['pseudo']) : "";
$message = isset($_POST['message']) ? htmlspecialchars($_POST['message']) : "";

if(!empty($pseudo) && !empty($message)) {
  $req = $bdd->prepare('INSERT INTO chat (pseudo, message ) VALUES(:pseudo, :message)');
  $req->execute(array(
    'pseudo' => $pseudo,
    'message' => $message
  ));

  echo 'Le message a été posté !';
  $_SESSION['erreur'] = "";
  $_SESSION['pseudo'] = $pseudo;
  $req->closeCursor();

}else {
  echo 'Le message est vide';
  $_SESSION['erreur'] = "erreur";
  $_SESSION['pseudo'] = "";

}
header('Location: index.php');
http://localhost:8888/PHP-1/TP/TP-6/index.php
?>