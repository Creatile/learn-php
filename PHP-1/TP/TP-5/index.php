<h2>Version PDO</h2>
<?php
try
{
  // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=jvideo;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
  // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}

// Si tout va bien, on peut continuer

// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM jeux_video');

// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{
  ?>
  <p>
    <strong>Jeu</strong> : <?php echo $donnees['nom']; ?><br />
    Le possesseur de ce jeu est : <?php echo $donnees['possesseur']; ?>, et il le vend à <?php echo $donnees['prix']; ?> euros !<br />
    Ce jeu fonctionne sur <?php echo $donnees['console']; ?> et on peut y jouer à <?php echo $donnees['nbre_joueurs_max']; ?> au maximum<br />
    <?php echo $donnees['possesseur']; ?> a laissé ces commentaires sur <?php echo $donnees['nom']; ?> : <em><?php echo $donnees['commentaires']; ?></em>
  </p>
  <?php
}

$reponse->closeCursor(); // Termine le traitement de la requête
?>

<h2>Version MYSQLi (PLUS RAPIDE)</h2>

<?php
// On se connecte à MySQL
$bdd = mysqli_connect('localhost', 'root', 'root', 'jvideo');

// Check connection
if ($bdd->connect_error) {
  die("Connection failed: " . $bdd->connect_error);
}
//echo "Connected successfully";

// On récupère tout le contenu de la table jeux_video

$reponse = mysqli_query($bdd, 'SELECT * FROM jeux_video');

// On affiche chaque entrée une à une

while($donnees = mysqli_fetch_assoc($reponse))
{
?>

<p>
  <strong>Jeu</strong> : <?php echo $donnees['nom']; ?><br />
  Le possesseur de ce jeu est : <?php echo $donnees['possesseur']; ?>, et il le vend à <?php echo $donnees['prix']; ?> euros !<br />
    Ce jeu fonctionne sur <?php echo $donnees['console']; ?> et on peut y jouer à <?php echo $donnees['nbre_joueurs_max']; ?> au maximum<br />
  <?php echo $donnees['possesseur']; ?> a laissé ces commentaires sur <?php echo $donnees['nom']; ?> : <em><?php echo $donnees['commentaires']; ?></em>
</p>

<?php
}

//$reponse->closeCursor(); // Termine le traitement de la requête
mysqli_close($bdd);

?>

<?php

try {
  $bdd = new PDO('mysql:host=localhost;dbname=jvideo;charset=utf8', 'root', 'root');
} catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}

$reponse = $bdd->query('SELECT nom, possesseur FROM jeux_video WHERE possesseur=\'Patrick\'');

while ($donnees = $reponse->fetch()) {
  echo $donnees['nom'] . ' appartient à ' . $donnees['possesseur'] . '<br />';
}

$reponse->closeCursor();

//Afficher des erreurs
$reponse = $bdd->query('SELECT nom FROM jex_video') or die(print_r($bdd->errorInfo()));

?>
