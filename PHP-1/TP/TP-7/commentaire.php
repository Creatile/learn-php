<h2>Commentaires</h2>
<?php
$req = $bdd->prepare('SELECT id, billet_id, auteur, commentaire, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%imin%ss\') AS date_commentaire_fr FROM commentaires WHERE billet_id = :billet_id');
$req->execute(array('billet_id' => $_GET['id']));
while ($donnees = $req->fetch())
{
  echo '<div>' . $donnees['id'] . '</div>';
  echo '<div>' . htmlspecialchars($donnees['auteur']) . '</div>';
  echo '<div>' . $donnees['date_commentaire_fr'] . '</div>';
  echo '<div>' . nl2br(htmlspecialchars($donnees['commentaire'])) . '</div>';
}

?>

<form method="POST" action="ajout_commentaire.php">
  <div class="form-item">
    <label for="auteur">Auteur</label>
    <input type="text" name="auteur" value="">
  </div>
  <div class="form-item">
    <label for="commentaire">Commentaire</label>
    <input type="textarea" name="commentaire" value="">
  </div>
  <div class="form-item">
    <label for="date_commentaire">Date</label>
    <input type="text" name="date_commentaire" value="<?php echo date('Y-m-d H:i:s'); ?>">
  </div>
  <div class="form-item">
    <label for="billet_id">Billet ID</label>
    <input type="number" name="billet_id" value="<?php echo $_GET['id']; ?>">
  </div>
  <div class=""><?php echo $_SESSION['erreur']; ?></div>

  <div class="form-action">
    <input type="submit" value="Ajouter">
  </div>

</form>

<?php
$req->closeCursor();
?>
