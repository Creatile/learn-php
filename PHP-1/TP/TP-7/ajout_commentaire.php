<?php
session_start();
try {
  $bdd = new PDO('mysql:host=localhost;dbname=blogphp;charset=utf8', 'root', 'root');
} catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}

$commentaire = isset($_POST['commentaire']) ? htmlspecialchars($_POST['commentaire']) : "";
$auteur = isset($_POST['auteur']) ? htmlspecialchars($_POST['auteur']) : "";
$billet_id = isset($_POST['billet_id']) ? htmlspecialchars($_POST['billet_id']) : "";
$date_commentaire = isset($_POST['date_commentaire']) ? htmlspecialchars($_POST['date_commentaire']) : "";

if (!empty($commentaire) && !empty($auteur) && !empty($billet_id) && !empty($date_commentaire)) {
  $req = $bdd->prepare('INSERT INTO commentaires (commentaire, auteur, billet_id, date_commentaire ) VALUES(:commentaire, :auteur, :billet_id, :date_commentaire)');
  $req->execute(array(
    'auteur' => $auteur,
    'commentaire' => $commentaire,
    'billet_id' => $billet_id,
    'date_commentaire' => $date_commentaire
  ));

  echo 'Le commentaire a été posté !';
  $_SESSION['erreur'] = "";
  $_SESSION['auteur'] = $auteur;
  $req->closeCursor();

} else {
  echo 'Le commentaire est vide';
  $_SESSION['erreur'] = "erreur";
  $_SESSION['auteur'] = "";

}
header('Location: article.php?id='.$_POST['billet_id'].'');
http://localhost:8888/PHP-1/TP/TP-6/index.php
