<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>HTML5 Starter Template</title>
  <meta name="description" content="Starter Template">
  <meta name="author" content="Gregry Pike">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" >
</head>
<body>
<h2>Billets de blog</h2>
<?php


try
{
  $bdd = new PDO('mysql:host=localhost;dbname=blogphp;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
  die('Erreur : '.$e->getMessage());
}

$req = $bdd->prepare('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billets WHERE id = :id');
$req->execute(array('id' => $_GET['id']));

while ($donnees = $req->fetch())
{

  echo '<div>' . htmlspecialchars($donnees['titre']) . '</div>';
  echo '<div>' . $donnees['date_creation_fr'] . '</div>';
  echo '<div>' . nl2br(htmlspecialchars($donnees['contenu']) ). '</div>';

}

$req->closeCursor();
?>
<?php include('commentaire.php'); ?>
</body>
</html>
