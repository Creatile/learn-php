<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>HTML5 Starter Template</title>
  <meta name="description" content="Starter Template">
  <meta name="author" content="Gregry Pike">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" >
</head>
<body>
<h2>Billets de blog</h2>
<?php
try
{
  $bdd = new PDO('mysql:host=localhost;dbname=blogphp;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
  die('Erreur : '.$e->getMessage());
}

$req = $bdd->prepare('SELECT b.titre titre,b.id id,  p.nom nom
FROM proprietaire p
INNER JOIN billets b
ON b.id_auteur = p.id');
$req->execute();
//LEFT OR RIGHT JOIN TO DISPLAY NULL VALUES

echo '<table>';
echo '<tbody>';

while ($donnees = $req->fetch())
{
  echo '<tr>';
  echo '<td><a href="article.php?id='.$donnees['id'].'">' . $donnees['titre'] . '</a></td>';
  echo '<td>test' . htmlspecialchars($donnees['nom']) . '</td>';
  echo '</tr>';
}

echo '</tbody>';
echo '</table>';

$req->closeCursor();

?>
</body>
</html>
