<?php include('../header.php'); ?>
  <title>Boucles</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Boucles While</h2>

<pre>
    <code>
    <?php
    $nombre_de_lignes = 1;

    while ($nombre_de_lignes <= 100)
    {
      echo 'Je ne dois pas regarder les mouches voler quand j\'apprends le PHP.<br />';
      $nombre_de_lignes++; // $nombre_de_lignes = $nombre_de_lignes + 1
    }
    ?>

    <?php
    $nombre_de_lignes = 1;

    while ($nombre_de_lignes <= 100)
    {
      echo 'Ceci est la ligne n°' . $nombre_de_lignes . '<br />';
      $nombre_de_lignes++;
    }
    ?>
    </code>
  </pre>
<h2>
  Boucles FOR
</h2>
<pre>
  <code>
    <?php
    for ($nombre_de_lignes = 1; $nombre_de_lignes <= 100; $nombre_de_lignes++)
    {
      echo 'Ceci est la ligne n°' . $nombre_de_lignes . '<br />';
    }
    ?>
  </code>
</pre>

<?php include('../footer.php'); ?>


