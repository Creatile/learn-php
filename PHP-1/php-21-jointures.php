<?php include('../header.php'); ?>
<title>Jointures PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Jointures PHP</h2>

<pre>
  <code>
    SELECT jeux_video.nom AS nom_jeu, proprietaires.prenom AS prenom_proprietaire
    FROM proprietaires, jeux_video
    WHERE jeux_video.ID_proprietaire = proprietaires.ID
  </code>
</pre>
<h2>Avec alias</h2>
<pre>
  <code>
    SELECT j.nom AS nom_jeu, p.prenom AS prenom_proprietaire
    FROM proprietaires AS p, jeux_video AS j
    WHERE j.ID_proprietaire = p.ID
  </code>
</pre>

<h2>Jointures Internes</h2>

<pre>
  <code>
    SELECT j.nom nom_jeu, p.prenom prenom_proprietaire
    FROM proprietaires p
    INNER JOIN jeux_video j
    ON j.ID_proprietaire = p.ID
    WHERE j.console = 'PC'
    ORDER BY prix DESC
    LIMIT 0, 10
  </code>
</pre>

<h2>Jointures externes</h2>
<pre>
  <code>
    SELECT j.nom nom_jeu, p.prenom prenom_proprietaire
    FROM proprietaires p
    LEFT JOIN jeux_video j
    ON j.ID_proprietaire = p.ID
  </code>
</pre>
<p>proprietaires  est appelée la « table de gauche » et jeux_video  la « table de droite ». Le LEFT JOIN  demande à récupérer tout le contenu de la table de gauche, donc tous les propriétaires, même si ces derniers n'ont pas d'équivalence dans la table jeux_video</p>


<?php include('../footer.php'); ?>



