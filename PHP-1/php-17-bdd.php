<?php include('../header.php'); ?>
<title>Base de donnée PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Base de donnée PHP</h2>

<pre>
  <code>
    <?php
    //version mysqli
    $bdd = mysqli_connect('serveur', 'utilisateur', 'motdepasse', 'basededonnes');
    $resultat = mysqli_query($bdd, 'SELECT * FROM table');
    echo 'Il y a '. mysqli_num_rows($result) . ' entrée(s) dans la base de données : </br>';
    while($donnees = mysqli_fetch_assoc($resultat)){
    echo $donnees['id'] . ' '.$donnees['message'].'</br>';
    } ?>

    <?php
    //version mysqli POO
    $bdd = new mysqli('serveur', 'utilisateur', 'motdepasse', 'basededonnes');
    $resultat = $bdd->query('SELECT * FROM table');
    echo 'Il y a '. $bdd->num_rows($result) . ' entrée(s) dans la base de données : </br>';
    while($donnees = $bdd->fetch_assoc($resultat)){
    echo $donnees['id'] . ' '.$donnees['message'].'</br>';
    } ?>

    <?php
    //version PDO
    try {
      $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
    } catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
    }

    ?>
  </code>
</pre>

<p>
  LIMIT 0, 20 : affiche les vingt premières entrées ;
</p>
<p>
  LIMIT 5, 10 : affiche de la sixième à la quinzième entrée ;
</p>
<p>
  LIMIT 10, 2 : affiche la onzième et la douzième entrée.
</p>
<h2>REQUETE PREPAREE</h2>
<pre>
  <code>
    <?php
    try
    {
      $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
      die('Erreur : '.$e->getMessage());
    }

    $req = $bdd->prepare('SELECT nom, prix FROM jeux_video WHERE possesseur = ?  AND prix <= ? ORDER BY prix');
    $req->execute(array($_GET['possesseur'], $_GET['prix_max']));

    echo '<ul>';
    while ($donnees = $req->fetch())
    {
      echo '<li>' . $donnees['nom'] . ' (' . $donnees['prix'] . ' EUR)</li>';
    }
    echo '</ul>';

    $req->closeCursor();

    ?>
    ou
    <?php
    $req = $bdd->prepare('SELECT nom, prix FROM jeux_video WHERE possesseur = :possesseur AND prix <= :prixmax');
    $req->execute(array('possesseur' => $_GET['possesseur'], 'prixmax' => $_GET['prix_max']));
    ?>
  </code>
</pre>


<?php include('../footer.php'); ?>
