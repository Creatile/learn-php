<?php include('../header.php'); ?>
<title>Dates PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Dates PHP</h2>

<pre>
  <code>
    SELECT pseudo, message, DAY(date) AS jour, MONTH(date) AS mois, YEAR(date) AS annee, HOUR(date) AS heure, MINUTE(date) AS minute, SECOND(date) AS seconde FROM minichat
  </code>
</pre>

<pre>
  <code>
  <?php
  echo $donnees['jour'] . '/' . $donnees['mois'] . '/' . $donnees['annee'] . '...';
  ?>
  </code>
</pre>
<h2>Date Format</h2
<pre>
  <code>
    SELECT pseudo, message, DATE_FORMAT(date, '%d/%m/%Y %Hh%imin%ss') AS date FROM minichat
  </code>
</pre>

<h2>Date expiration</h2>
<pre>
  <code>
    SELECT pseudo, message, DATE_ADD(date, INTERVAL 2 MONTH) AS date_expiration FROM minichat
  </code>
</pre>



<?php include('../footer.php'); ?>



