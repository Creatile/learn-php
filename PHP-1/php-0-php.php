<?php include('../header.php'); ?>
    <title>HTML-CSS-PHP</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="../style.css">
<link rel="stylesheet" href="../prism/prism.css">
<script src="../prism/prism.js"></script>

  </head>
  <body>
  <p>Code HTML</p>
  <pre>
    <code class="hljs language-html">
      <p>Bonjour, je suis un <em>paragraphe</em> de texte !</p>
    </code>
  </pre>

    <p>code css</p>

    <pre>
    <code class="language-css">
      .banner {
        color: #FFFFFF;
        background-color: #000000;
      }
    </code>
  </pre>

  <p>code PHP</p>

  <pre>
    <code class="language-php">
      <?php
      $name = "Guillaume";
      echo "bonjour ". $name;
      ?>
    </code>
  </pre>
  <?php include('../footer.php'); ?>
