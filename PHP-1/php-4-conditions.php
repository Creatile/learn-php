<?php include('../header.php'); ?>
  <title>Conditions</title>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Conditions</h2>

  <pre>
    <code>
      <?php
      $age = 8;

      if ($age <= 12)
      {
        echo "Salut gamin !";
      }
      ?>
    </code>
  </pre>
<pre>
  <code>
    <?php
    $age = 8;

    if ($age <= 12) // SI l'âge est inférieur ou égal à 12
    {
      echo "Salut gamin ! Bienvenue sur mon site !<br />";
      $autorisation_entrer = "Oui";
    }
    else // SINON
    {
      echo "Ceci est un site pour enfants, vous êtes trop vieux pour pouvoir  entrer. Au revoir !<br />";
      $autorisation_entrer = "Non";
    }

    echo "Avez-vous l'autorisation d'entrer ? La réponse est : $autorisation_entrer";
    ?>
  </code>
</pre>
<p>
  Booléens
</p>
<pre>
  <code>
    <?php
    $autorisation_entrer = true;

    if ($autorisation_entrer == true)
    {
      echo "Bienvenue petit nouveau. :o)";
    }
    elseif ($autorisation_entrer == false)
    {
      echo "T'as pas le droit d'entrer !";
    }
    ?>

    <?php
    $autorisation_entrer = true;

    if (! $autorisation_entrer)
    {

    }
    ?>
  </code>
</pre>
<p>Conditions multiples</p>
<pre>
  <code>
    <?php
    $age = 8;
    $langue = "anglais";


    if ($age <= 12 AND $langue == "français")
    {
      echo "Bienvenue sur mon site !";
    }
    elseif ($age <= 12 AND $langue == "anglais")
    {
      echo "Welcome to my website!";
    }
    ?>

    <?php
    $variable = 23;

    if ($variable == 23)
    {
      ?>
      <strong>Bravo !</strong> Vous avez trouvé le nombre mystère !
      <?php
    }
    ?>
  </code>
</pre>

<h2>Switch</h2>
<pre>
  <code>
    <?php
    $note = 10;

    switch ($note) // on indique sur quelle variable on travaille
    {
      case 0: // dans le cas où $note vaut 0
        echo "Tu es vraiment un gros nul !!!";
        break;

      case 5: // dans le cas où $note vaut 5
        echo "Tu es très mauvais";
        break;

      case 7: // dans le cas où $note vaut 7
        echo "Tu es mauvais";
        break;

      case 10: // etc. etc.
        echo "Tu as pile poil la moyenne, c'est un peu juste…";
        break;

      case 12:
        echo "Tu es assez bon";
        break;

      case 16:
        echo "Tu te débrouilles très bien !";
        break;

      case 20:
        echo "Excellent travail, c'est parfait !";
        break;

      default:
        echo "Désolé, je n'ai pas de message à afficher pour cette note";
    }
    ?>
  </code>
</pre>
<p>break; permet de sortir du switch</p>
<h2>COnditions ternaires</h2>
<pre>
  <code>
    <?php
    $age = 24;

    $majeur = ($age >= 18) ? true : false;
    ?>
  </code>
</pre>

<?php include('../footer.php'); ?>

