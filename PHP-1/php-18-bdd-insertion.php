<?php include('../header.php'); ?>
<title>Base de donnée insertion PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Base de donnée insertion PHP</h2>

<pre>
  <code>
    <?php
    try
    {
      $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
      die('Erreur : '.$e->getMessage());
    }

    // On ajoute une entrée dans la table jeux_video
    $bdd->exec('INSERT INTO jeux_video(nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(\'Battlefield 1942\', \'Patrick\', \'PC\', 45, 50, \'2nde guerre mondiale\')');

    echo 'Le jeu a bien été ajouté !';
    ?>

    ou

    <?php
    $req = $bdd->prepare('INSERT INTO jeux_video(nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(:nom, :possesseur, :console, :prix, :nbre_joueurs_max, :commentaires)');
    $req->execute(array(
      'nom' => $nom,
      'possesseur' => $possesseur,
      'console' => $console,
      'prix' => $prix,
      'nbre_joueurs_max' => $nbre_joueurs_max,
      'commentaires' => $commentaires
    ));

    echo 'Le jeu a bien été ajouté !';
    ?>
  </code>
</pre>

<h2>UPDATE</h2>
<pre>
  <code>
    <?php
    $req = $bdd->prepare('UPDATE jeux_video SET prix = :nvprix, nbre_joueurs_max = :nv_nb_joueurs WHERE nom = :nom_jeu');
    $req->execute(array(
      'nvprix' => $nvprix,
      'nv_nb_joueurs' => $nv_nb_joueurs,
      'nom_jeu' => $nom_jeu
    ));
    ?>
  </code>
</pre>

<h2>ERREURS</h2>
<pre>
  <code>
    <?php
    $reponse = $bdd->query('SELECT nom FROM jeux_video') or die(print_r($bdd->errorInfo()));
    ?>
  </code>
</pre>



<?php include('../footer.php'); ?>
