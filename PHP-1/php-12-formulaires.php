<?php include('../header.php'); ?>
<title>Formulaires PHP</title>
<meta charset="utf-8"/>
<?php include('../scripts.php'); ?>
</head>
<body>
<h2>Formulaires PHP</h2>
<pre>
  <code>
    <p>
    Cette page ne contient que du HTML.<br />
    Veuillez taper votre prénom :
    </p>

    <form action="cible.php" method="post">
    <p>
        <input type="text" name="prenom" />
        <input type="submit" value="Valider" />
    </p>
    </form>
  </code>
</pre>
<h2>Sécuriser</h2>
<pre>
  <code>
    <p>Je sais comment tu t'appelles, hé hé. Tu t'appelles <?php echo htmlspecialchars($_POST['prenom']); ?> !</p>
  </code>
</pre>



<?php include('../footer.php'); ?>
