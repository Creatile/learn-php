<?php include('../header.php'); ?>
  <title>Notre première instruction : echo</title>
  <meta charset="utf-8"/>
  <?php include('../scripts.php'); ?>
</head>
<body>
<h2>Balises</h2>
<pre>
  <code>
    <?php
    /* Le code PHP se met ici
    Et ici
    Et encore ici */
    ?>
  </code>
</pre>

<h2>Instruction ECHO</h2>
<pre>
  <code>
    <?php echo "Ceci est du texte"; ?>
    <?php echo "Cette ligne a été écrite \"uniquement\" en PHP."; ?>
    <p>Aujourd'hui nous sommes le <?php echo date('d/m/Y h:i:s'); ?>.</p>
    <?php
    echo "J'habite en Chine."; // Cette ligne indique où j'habite

    // La ligne suivante indique mon âge
    echo "J'ai 92 ans.";
    ?>
  </code>
</pre>


<?php include('../footer.php'); ?>

